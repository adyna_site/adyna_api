const Email = require("../../utils/email");
module.exports = function (app) {
  //send an email with instructions to reset an existing user's password
  app.post('/request-password-reset', function (req, res, next) {
    User.resetPassword({
      email: req.body.email
    }, async function (err) {
      if (err) return res.status(401).send(err);
      console.log(req.body);

      await new Email(
        info,
        "localhost:3000/passwordreset"
      ).sendPasswordReset();
      res.status(200).json({
        status: "success",
        data: {

        }
      });
      // res.render('response', {
      //   title: 'Password reset requested',
      //   content: 'Check your email for further instructions',
      //   redirectTo: '/',
      //   redirectToLinkText: 'Log in'
      // });
    });
  });
}
