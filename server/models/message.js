"use strict";
const Email = require("../../utils/email");

module.exports = function (Message) {
  Message.sendEmail = async function (emailInfo) {
    // console.log(emailInfo);

    await new Email(emailInfo, "www.bambut.com").sendCustomEmail(
      emailInfo.subject,
      emailInfo.text
    );
    // return "sent";
  };
  Message.remoteMethod("sendEmail", {
    description: "Send email for user",
    accepts: {
      arg: "emailInfo",
      type: "Object",
      required: true,
      http: {
        source: "query",
      },
    },
    // returns: {
    //   arg: "status",
    //   type: "string",
    //   root: true,
    // },
    http: {
      verb: "get",
      path: "/send-email-for-Customer",
    },
  });
};
